/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.junit.Test;
import static org.junit.Assert.*;

import vk4.pkg1.Debugging.Point;


/**
 *
 * @author otson
 */
public class TestDebugger {
    
      @Test
public void testmovePointRightBy()
throws Exception {
Point point = new Point(5,5);
Point expectedPoint = new Point(15,5);
Point newPoint = point.moveRightBy(10);
assertEquals(expectedPoint.getX(), newPoint.getX());
}
}
