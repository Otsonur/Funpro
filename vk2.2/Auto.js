'use strict';

const Auto = (function(){

    const suojatut = new WeakMap();  // yhteinen ilmentymille

    class Auto{
        constructor(p_tankki, p_matkamittari){
            suojatut.set(this, {tankki: p_tankki, matkamittari: p_matkamittari});
        }  // suojatut instanssimuuttujat nimi ja ika


        getTankki() {return suojatut.get(this).tankki;}

        getMatkamittari(){return suojatut.get(this).matkamittari;}

    }

    return Auto;
})();


const auto1 = new Auto(12, 1500 );
console.log("Tankki "+ auto1.getTankki());
console.log("Matkamittari "+auto1.getMatkamittari());

