'use strict';





    function Auto(tankki, matkamittari) {
        let _matkamittari = matkamittari;
        let _tankki = tankki;
        return {
            matkamittari: function () { // anonyymi funktio objektin ominaisuutena
                return _matkamittari;
            },
            tankki: function () {
                return _tankki;
            }


        };

    }





const auto1 = Auto(12, 1500);
console.log("Tankki " + auto1.tankki());
console.log("Matkamittari " + auto1.matkamittari());
